-- find all artists that has letter D in its name
SELECT * FROM artists WHERE name LIKE "%D%";

-- find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- joining "albums" and "songs" 
--Only show the album name, song name and song length
SELECT album_title as album_name, song_name, length as
song_length FROM albums JOIN songs WHERE albums.id = songs.album_id;

-- joining "artists" and "albums"
-- find all albums that has letter A in it
SELECT * FROM artists JOIN albums
ON artists.id = albums.artist_id WHERE artists.name LIKE '%A%';

-- sort the albums in Z-A and LIMIT to 4
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- joining "albums" and "songs" 
-- sort albums to DESC and sort songs to ASC
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC, songs.song_name ASC;
